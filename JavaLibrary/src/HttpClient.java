
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;


/**
 *
 * @author Lenovo
 */
public class HttpClient
{
    String              sUserAgent  = null;
    String              sPostParm   = null;    
    HttpsURLConnection  httpscon    = null;
    HttpURLConnection   httpcon     = null;
    URL                 uUrl        = null;     

    public HttpClient()
    {
        this.sUserAgent = getUserAgent();
    }    

    public HttpClient(String sUserAgent)
    {
        this.sUserAgent = sUserAgent;
    }
    
    public String POST(String sUrl, String sPostData)
    {
        try
        {
            uUrl = new URL(sUrl);
        }
        catch (MalformedURLException ex)
        {
            Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        switch(checkUrl(sUrl))
        {
            case 1:
                try
                {
                    httpscon = (HttpsURLConnection)uUrl.openConnection();
                }
                catch (IOException ex)
                {
                    Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                try
                {
                    httpscon.setRequestMethod("POST");
                    httpscon.setRequestProperty("User-Agent", sUserAgent);
                }
                catch (ProtocolException ex)
                {
                    Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 2:
                try
                {
                    httpcon = (HttpURLConnection)uUrl.openConnection();
                }
                catch (IOException ex)
                {
                    Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                try
                {
                    httpcon.setRequestMethod("GET");
                }
                catch (ProtocolException ex)
                {
                    Logger.getLogger(HttpClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                        httpcon.setRequestProperty("User-Agent", sUserAgent);
                        break;
        }
        
        return null;
    }
    
    private int checkUrl(String sUrl)
    {
        if(sUrl.matches("https"))
        {
            return 1;
        }
        else if(sUrl.matches("http"))
        {
            return 2;
        }
        return 0;
    }
    
    private String getUserAgent()
    {
        ArrayList<String> listUSerAgent = new ArrayList<>();
        int iLow = 0;
        Random r = new Random();
        int iUA;
        
        //Chrome 35.0.1916.114
        listUSerAgent.add("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36");
        
        //Firefox 31.0
        listUSerAgent.add("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0");
        
        //Internet Explorer 11
        listUSerAgent.add("Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko");
        
        iUA = r.nextInt(listUSerAgent.size()-iLow) + iLow;
        
        return listUSerAgent.get(iUA);
    }
        
}
